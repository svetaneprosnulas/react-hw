import React from 'react';
import '../footer/footer.css'


const Footer = () => {
    return (
        <div className="footer">
            <p>Copyright by phototime - all right reserved</p>
        </div>
    )
}

export default Footer