import React from 'react';
import ReactDOM from 'react-dom';
import Header from './components/header/header'
import Photo from './components/photo/photo'
import Heading from './components/heading/heading'
import Text from './components/text/text'
import Button from './components/button/button'
import Footer from './components/footer/footer'

ReactDOM.render(<Header></Header>, document.querySelector(".header"))
ReactDOM.render(<Photo></Photo>, document.querySelector(".photo"))
ReactDOM.render(<Heading></Heading>, document.querySelector('.heading'))
ReactDOM.render(<Text></Text>, document.querySelector(".text"))
ReactDOM.render(<Button></Button>, document.querySelector(".button"))
ReactDOM.render(<Footer></Footer>, document.querySelector(".footer"))